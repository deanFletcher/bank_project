import dbm

import bcrypt
import shelve


class UserAccountDetails:

    def __init__(self, userName, password, current_balance=0):
        self.userName = userName
        self.password = self.hash_password(password)
        self.current_balance = current_balance
        self.save_to_shelve()

    @classmethod
    def authenticate_user(cls, userName, password):
        try:
            with shelve.open('user_data.db', 'r') as user_data:
                if userName in user_data:
                    user_record = user_data[userName]
                    hashed_password = user_record.get('password')
                    if bcrypt.checkpw(password.encode('utf-8'), hashed_password):
                        return user_record
                else:
                    return None
        except dbm.error:
            return None

    def hash_password(self, password):
        salt = bcrypt.gensalt()
        hashed = bcrypt.hashpw(password.encode('utf-8'), salt)
        return hashed

    def save_to_shelve(self):
        with shelve.open('user_data.db', writeback=True) as user_data:
            user_data[self.userName] = {'password': self.password, 'current_balance': self.current_balance,
                                        'user_name': self.userName}

    @classmethod
    def get_user_details(cls, userName):
        with shelve.open('user_data.db', 'r') as user_data:
            if userName in user_data:
                return user_data[userName]

    @classmethod
    def update_balance(cls, user_instance, method, amount):
        with shelve.open('user_data.db', writeback=True) as user_data:
            current_balance = user_data[user_instance]['current_balance']
            if method == 'deposit':
                updated_amt = current_balance + amount
                user_data[user_instance]['current_balance'] = updated_amt
                updated_balance = user_data[user_instance]['current_balance']
                return amount, updated_balance
            elif method == 'withdraw':
                if (current_balance - amount) < 0:
                    return -1, current_balance
                else:
                    updated_amt = current_balance - amount
                    user_data[user_instance]['current_balance'] = updated_amt
                    updated_balance = user_data[user_instance]['current_balance']
                    return amount, updated_balance

