import sys

from account_management import create_account, check_account_verify, login
from user_actions import deposit_money, withdraw_money, action_verify, check_balance


def banking():
    user_instance = None
    print("Welcome to Awesome-o's Bank of Totally Not Money Laundering.")
    has_account = check_account_verify()
    if has_account == 'no':
        user_instance = create_account()
    elif has_account == 'yes':
        user_instance = login()
    # Ensure user_instance is not None before proceeding

    if user_instance is not None:
        # Main account action flow
        while True:
            user_action = action_verify()
            if user_action == 'deposit money':
                deposit_money(user_instance['user_name'])
            elif user_action == 'withdraw money':
                withdraw_money(user_instance['user_name'])
            elif user_action == 'check balance':
                check_balance(user_instance)
            elif user_action == 'log off':
                sys.exit("Exiting Awesome-o's banking application. Have a good day laundering!")

banking()
