import bcrypt

from UserAccountDetails import UserAccountDetails
import shelve


def create_account():
        userName = input('What would you like your new account user name to be? ')
        password = input('What would you like your new password to be? ')
        UserAccountDetails(userName, password, current_balance=0)
        user_info = UserAccountDetails.get_user_details(userName)
        print(f'Account successfully created! Be sure to write down your username of {user_info.get("user_name")}.')
        return user_info
        pass


def check_account_verify():
    while True:
        account_exists = input("Do you have an account with us, yes or no? ").lower()
        if account_exists in ('yes', 'no'):
            return account_exists
        else:
            print("Invalid Input: Response should be 'yes' or 'no'.")


def auth_login():
    inputUserName = input('What is your username? ')
    inputPassword = input('What is your password? ')
    userNameAuthenticated = UserAccountDetails.authenticate_user(inputUserName, inputPassword)
    if userNameAuthenticated:
        return True, userNameAuthenticated
    else:
        return False, userNameAuthenticated


def login():
    authenticated, user_details = auth_login()
    if authenticated:
        print(f'Welcome back {user_details["user_name"]}.')
        return user_details
    else:
        print('We do not have any accounts with those login details, please create a new account: ')
        create_account()
