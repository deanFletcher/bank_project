from UserAccountDetails import UserAccountDetails


def deposit_money(user_instance):
    dep_amt = get_int_input('How many USD would you like to deposit into your account? ')
    try:
        amount, updated_balance = UserAccountDetails.update_balance(user_instance, 'deposit', dep_amt)
        print(f'You successfully deposited {amount} USD. \nYour New Account Balance is: ${updated_balance}.')
        return
    except Exception as e:
        print(f'You encountered an error while making a deposit: {e}, please try again.')


def withdraw_money(user_instance):
    while True:
        with_amt = get_int_input('How many USD would you like to withdraw from your account? ')
        try:
            amount, updated_balance = UserAccountDetails.update_balance(user_instance, 'withdraw', with_amt)
            if amount == -1:
                print(f'This withdraw of {with_amt} exceeds your account balance of ${updated_balance}. Transaction Cancelled')
            else:
                print(f'You successfully withdrew ${amount}. \nYour new account balance is ${updated_balance}')
            break
        except Exception as e:
            print(f'You encountered an error while making a withdraw: {e}, please try again.')


def check_balance(user_instance):
    user_account = UserAccountDetails.get_user_details(user_instance.get('user_name'))
    print(f'You Account Balance is ${user_account.get("current_balance")}.')


def get_int_input(prompt):
    while True:
        try:
            if int(input(prompt)) >= 0:
                print(int(input(prompt)))
                return int(input(prompt))
            else:
                print(int(input(prompt)))
                print('Withdraw amount cannot be negative, consider depositing instead.')
        except ValueError:
            print('Amount must be integers. Please try again: ')


def action_verify():
    while True:
        user_action = input('\nSelect an account action from the following list:'
                            '\nDeposit Money. \nWithdraw Money. \nCheck Balance. \nLog Off. \nWhat would you like to do? ').lower()
        if user_action in ('deposit money', 'withdraw money', 'check balance', 'log off'):
            return user_action
        else:
            print('Invalid account action, please try again.')
